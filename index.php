<?php
session_start();
require_once './vendor/autoload.php';

use App\User\User;

//use \PDO;
$login = new User();

if ($login->is_loggedin() != "") {
    $login->redirect('./view/User/home.php');
}

if (isset($_POST['btn-login'])) {
    $uname = strip_tags($_POST['username_email']);
    $umail = strip_tags($_POST['username_email']);
    $upass = strip_tags($_POST['password']);


    if ($login->doLogin($uname, $umail, $upass)) {
        $login->redirect('./view/User/home.php');
    } else {
        $error = "Wrong Details !";
    }
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Login</title>

        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" id="bootstrap-css">
        <link href="assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/app.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Hind+Siliguri:400,300,500,600,700&subset=bengali,latin' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>

        <div class="signin-form">

            <div class="container">


                <form class="form-signin" method="post" id="login-form">

                    <h4 style="font-family: 'Hind Siliguri', sans-serif;font-weight: 300" class="form-signin-heading text-center"><?php echo "আজ " . "  " . date("d-m-Y") . "  " . date("l"); ?></h4>
                    <h1 style="font-family: 'Hind Siliguri', sans-serif;font-weight: 300" class="form-signin-heading text-center">স্বাগতম </h1>

                    <h3 style="font-family: 'Hind Siliguri', sans-serif;font-weight: 300" class="form-signin-heading text-center">Debug লগইন  .</h3><hr />


                    <div id="error">
<?php
if (isset($error)) {
    ?>
                            <div class="alert alert-danger">
                                <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="username_email" placeholder="Username or E mail ID" required />
                        <span id="check-e"></span>
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Your Password" />
                    </div>

                    <hr />

                    <div class="form-group">
                        <button type="submit" name="btn-login" class="btn btn-default">
                            <i class="glyphicon glyphicon-log-in"></i> &nbsp; SIGN IN
                        </button>
                    </div>  
                    <br />
                    <label>Don't have account yet ! <a href="view/User/sign-up.php">Sign Up</a></label>
                </form>

            </div>

        </div>


    </body>
</html>
